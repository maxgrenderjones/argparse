import hashes
import os
import sequtils
import strutils
import tables
import std/wordwrap 

## :Author: Max Grender-Jones
## Module inspired by python's argparse to provide simple commandline parsing

const CommandParserDebug = when isMainModule: false else: false

const TRUEVALUES = @["yes", "on", "true", "1"]
const FALSEVALUES = @["no", "off", "false", "0"]

type
  Command* = distinct string
  Count* = distinct int
  Help* = distinct bool

  Value = int|float|bool|string|Command|Count|Help

  Parser* = ref ParserObj ## The parser object

  ParserObj = object
    arguments: seq[Option]
    options: OrderedTable[string, Option]
    prolog: string
    epilog: string
    help: string
    longPrefix: string
    shortPrefix: char
    leftBracket: char
    rightBracket: char
  
  ParseResult = tuple[arguments: seq[Option], options: OrderedTable[string, Option], help: bool]

  ParseError* = object of CatchableError

  OptionKind = enum
    okPositional,
    okOption,
    okCommand

  ValueKind = enum
    vkInt, # Value is an integer
    vkFloat, # Value is a float
    vkString, # Value is a string
    vkCount, # Counts the number of times it appears
    vkBool, # True or False value
    vkCommand, # Subcommand
    vkHelp # Presence should trigger help message

  RequiredKind* = enum ## Controls whether the option/argument is required or optional
    rkRequired, ## Option must appear
    rkOptional, ## Option is optional
    rkDefault ## Default behaviour (options are optional, positionals are required)

  Option = ref OptionObj

  OptionObj = object
    variants: seq[string]
    help: string
    name: string
    n: int
    count: int
    required: RequiredKind
    optionKind: OptionKind
    case valueKind: ValueKind
      of vkInt:
        intVal: int
        intVals: seq[int]
        intChoices: seq[int]
        defaultInt: int
      of vkString:
        stringVal: string
        stringVals: seq[string]
        stringChoices: seq[string]
        defaultString: string
      of vkCount:
        countVal: int
        defaultCount: int
      of vkBool:
        boolVal: bool
        boolVals: seq[bool]
        boolChoices: seq[bool]
        defaultBool: bool
      of vkFloat:
        floatVal: float
        floatVals: seq[float]
        floatChoices: seq[float]
        defaultFloat: float
      of vkCommand:
        commandVal: Command
        parsers: OrderedTable[string, Parser]
        parsed: Table[string, ParseResult]
        defaultCommand: Command
      of vkHelp:
        helpVal: bool
        defaultHelp: bool

proc `$`*(c: Command): string {.borrow.}

proc `$`*(c: Count): string {.borrow.}

proc newParser*(prolog, epilog, help: string = "", addHelp = true, longPrefix="--", shortPrefix='-',
                  leftBracket='<', rightBracket='>'): Parser =
  ## Creates a new parser
  ## - ``prolog`` is the opening part of the help text
  ## - ``epilog`` is the closing part of the help test
  ## - ``longPrefix`` introduces multi-character options
  ## - ``shortPrefix`` introduces single-character options
  new(result)
  result.options = initOrderedTable[string, Option]()
  result.arguments = newSeq[Option]()
  result.prolog = prolog
  result.epilog = epilog
  result.help = help
  if longPrefix == $shortPrefix:
    raise newException(ValueError, "Long and short option cannot be the same")
  result.longPrefix = longPrefix
  result.shortPrefix = shortPrefix
  result.leftBracket = leftBracket
  result.rightBracket = rightBracket

proc addOption(parser: var Parser, option: Option) =
  ## Used during parser defintion to add a newly created option to the Parser.
  ## By now, option should have passed all integrity checks
  for variant in option.variants:
    if parser.options.hasKey(variant):
      raise newException(ValueError, "Parser already has a " & variant & " option defined")
  case option.optionKind:
    of OptionKind.okPositional, OptionKind.okCommand:
      case option.required:
        of RequiredKind.rkDefault, RequiredKind.rkRequired:
          if len(parser.arguments)>0 and parser.arguments[len(parser.arguments)-1].required == RequiredKind.rkOptional:
            raise newException(ValueError, "Cannot have required arguments after optional ones")
        else:
          discard
    else:
      discard
  for variant in option.variants:
    when CommandParserDebug:
      echo("Adding variant: " & variant)
    parser.options[variant] = option
  case option.optionKind:
    of OptionKind.okPositional, OptionKind.okCommand:
      when CommandParserDebug:
        echo("Adding positional argument: " & option.name)
      parser.arguments.add(option)
    else:
      discard

proc addValue(option: var Option, value: string) {.raises: [ParseError].}=
  ## Adds a string value to the given option
  case option.valueKind:
    of ValueKind.vkString:
      if len(option.stringChoices)>0 and not (value in option.stringChoices):
        raise newException(ParseError, "Invalid argument " & value & ": expected one of [" & option.stringChoices.join(",") & "]")
      option.stringVal = value
      option.stringVals.add(value)
    of ValueKind.vkInt:
      try:
        let intVal = parseInt(value)
        if len(option.intChoices)>0 and not (intVal in option.intChoices):
          let choicesAsStrings = map(option.intChoices, proc(x: int): string = $x)
          raise newException(ParseError, "Invalid argument " & value & ": expected one of [" & choicesAsStrings.join(",") & "]")
        option.intVal = intVal
        option.intVals.add(intVal)
      except ValueError:
        raise newException(ParseError, "Could not parse int value " & value & ": " & getCurrentExceptionMsg())
    of ValueKind.vkFloat:
      try:
        let floatVal = parseFloat(value)
        if len(option.floatChoices) > 0 and not (floatVal in option.floatChoices):
          let choicesAsStrings = map(option.floatChoices, proc(x: float): string = $x)
          raise newException(ParseError, "Invalid argument " & value & ": expected one of [" & choicesAsStrings.join(",") & "]")
        option.floatVal = floatVal
        option.floatVals.add(floatVal)
      except ValueError:
        raise newException(ParseError, "Could not parse int value " & value & ": " & getCurrentExceptionMsg())
    of ValueKind.vkBool:
      if value.toLower() in TRUEVALUES:
        option.boolVal = true
      elif value.toLower() in FALSEVALUES:
        option.boolVal = false
      else:
        raise newException(ParseError, "Invalid true/false value: " & value)
      option.boolVals.add(option.boolVal)
    of ValueKind.vkCommand:
      let commandVal = Command(value)
      if not (value.toLower() in option.parsers):
        raise newException(ParseError, "Invalid argument " & value & ": expected one of [" & toSeq(option.parsers.keys).join(", ") & "]")
      option.commandVal = commandVal
    of ValueKind.vkCount, ValueKind.vkHelp:
      raise newException(ParseError, "Only strings, ints and bools can take values")

proc default(option: Option): string =
  ## Returns the default value of an option as a string
  result = case option.valueKind:
            of vkString: option.defaultString
            of vkInt: $option.defaultInt
            of vkFloat: $option.defaultFloat
            of vkBool: $option.defaultBool
            of vkCount: $option.defaultCount
            of ValueKind.vkCommand: $option.defaultCommand
            of vkHelp: $option.defaultHelp

proc values(option: Option): seq[string] =
  case option.valueKind:
    of vkString:
      result = option.stringVals
    of vkInt:
      result = map(option.intVals, proc (x: int): string = $x)
    of vkFloat:
      result = map(option.floatVals, proc (x: float): string = $x)
    of vkBool:
      result = map(option.boolVals, proc (x: bool): string = $x)
    of vkCount:
      result = @[$option.countVal]
    of vkCommand:
      result = @[$option.commandVal]
    of vkHelp:
      result = @[$option.helpVal]

proc hasOption*(parser: ParseResult, variant: string): bool =
  ## Returns true if parser has a variant option
  if not parser.options.hasKey(variant):
    raise newException(KeyError, "Parser does not have a '" & variant & "' option")
  result = parser.options[variant].count>0

proc get*[T: Value](parse_result: ParseResult, variant: string): T =
  ## Gets a value from the ParseResult of the appropriate type
  if not parse_result.options.hasKey(variant):
    raise newException(ValueError, "No such option: " & variant)
  let option = parse_result.options[variant]
  when T is int:
    if option.valueKind != ValueKind.vkInt:
      raise newException(ValueError, variant & " is not an int option")
    result = if option.count>0: option.intVal else: option.defaultInt
  elif T is float:
    if option.valueKind != ValueKind.vkFloat:
      raise newException(ValueError, variant & " is not a float option")
    result = if option.count>0: option.floatVal else: option.defaultFloat
  elif T is bool:
    if option.valueKind != ValueKind.vkBool:
      raise newException(ValueError, variant & " is not a bool option")
    result = if option.count>0: option.boolVal else: option.defaultBool
  elif T is string:
    if option.valueKind != ValueKind.vkString:
      raise newException(ValueError, variant & " is not a string option")
    result = if option.count>0: option.stringVal else: option.defaultString
  elif T is Command:
    if option.valueKind != ValueKind.vkCommand:
      raise newException(ValueError, variant & " is not a command option")
    result = if option.count>0: option.commandVal else: option.defaultCommand
  elif T is Count:
    if option.valueKind != ValueKind.vkCount:
      raise newException(ValueError, variant & " is not a count option")
    result = Count(if option.count>0: option.count else: option.defaultCount)
  else:
    {.fatal: "Unhandled type".}

proc gets*[T: Value](parse_result: ParseResult, variant: string): seq[T] =
  ## Gets a sequence of values from the ParseResult of the appropriate type
  if not parse_result.options.hasKey(variant):
    raise newException(ValueError, "No such option: " & variant)
  let option = parse_result.options[variant]
  when T is int:
    if option.valueKind != ValueKind.vkInt:
      raise newException(ValueError, variant & " is not an int option")
    result = if option.count>0: option.intVals else: @[option.defaultInt]
  elif T is float:
    if option.valueKind != ValueKind.vkFloat:
      raise newException(ValueError, variant & " is not a float option")
    result = if option.count>0: option.floatVals else: @[option.defaultFloat]
  elif T is bool:
    if option.valueKind != ValueKind.vkBool:
      raise newException(ValueError, variant & " is not a bool option")
    result = if option.count>0: option.boolVals else: @[option.defaultBool]
  elif T is string:
    if option.valueKind != ValueKind.vkString:
      raise newException(ValueError, variant & " is not a string option")
    result = if option.count>0: option.stringVals else: @[option.defaultString]
  else:
    {.fatal: "Unhandled type".}

proc getInt*(parse_result: ParseResult, variant: string): int =
  result = get[int](parse_result, variant)

proc getFloat*(parse_result: ParseResult, variant: string): float =
  result = get[float](parse_result, variant)

proc getString*(parse_result: ParseResult, variant: string): string =
  result = get[string](parse_result, variant)

proc getBool*(parse_result: ParseResult, variant: string): bool =
  result = get[bool](parse_result, variant)

proc getCommand*(parse_result: ParseResult, variant: string): string =
  result = $get[Command](parse_result, variant)

proc getParseResult*(parse_result: ParseResult, variant: string): ParseResult =
  let command = getCommand(parse_result, variant) # Checks variant is a valid command option
  result = parse_result.options[variant].parsed[command] # TODO: this could raise a KeyError

proc getCount*(parse_result: ParseResult, variant: string): int =
  result = int(get[Count](parse_result, variant))

proc getInts*(parse_result: ParseResult, variant: string): seq[int] =
  result = gets[int](parse_result, variant)

proc getFloats*(parse_result: ParseResult, variant: string): seq[float] =
  result = gets[float](parse_result, variant)

proc getStrings*(parse_result: ParseResult, variant: string): seq[string] =
  result = gets[string](parse_result, variant)

proc getBools*(parse_result: ParseResult, variant: string): seq[bool] =
  result = gets[bool](parse_result, variant)

proc optionString(parser: Parser, option: Option): string =
  result = option.variants.join(", ")
  if option.variants[0].startsWith($parser.shortPrefix) or option.variants[0].startsWith(parser.longPrefix):
    case option.valueKind:
      of ValueKind.vkString, ValueKind.vkInt, ValueKind.vkCommand:
        result &= " " & parser.leftBracket & option.name & parser.rightBracket
      else:
        discard

proc optionHelp(option: Option): string =
  result = ""
  if len(option.help)>0:
    result &= option.help & " "
  # If the option is optional then the default value is relevant
  if option.required == rkOptional or (option.optionKind == okOption and option.required == rkDefault):
    result &= "[default: " & $option.default & "]"
  
proc newOption[T: Value|Command](parser: Parser, variants: seq[string], help: string, default: T,
                    choices: seq[T], name: string, n: int, required: RequiredKind): Option =
  # Type-Specific setup
  when T is int:
    result = Option(valueKind: ValueKind.vkInt)
    result.defaultInt=default
    result.intVals=newSeq[int]()
    result.intChoices=choices
  elif T is string:
    result = Option(valueKind: ValueKind.vkString)
    result.defaultString=default
    result.stringVals=newSeq[string]()
    result.stringChoices=choices
  elif T is bool:
    result = Option(valueKind: ValueKind.vkBool)
    result.defaultBool=default
    result.boolVals=newSeq[bool]()
    result.boolChoices=choices
  elif T is float:
    result = Option(valueKind: ValueKind.vkFloat)
    result.defaultFloat=default
    result.floatVals=newSeq[float]()
    result.floatChoices=choices
  elif T is Count:
    result = Option(valueKind: ValueKind.vkCount)
    result.defaultCount=int(default)
  elif T is Command:
    result = Option(valueKind: ValueKind.vkCommand)
    result.defaultCommand=default
    result.parsed=initTable[string, ParseResult]()
  elif T is Help:
    result = Option(valueKind: ValueKind.vkHelp)
    result.defaultHelp=bool(default)
  else:
    {.fatal: "Unhandled type".}

  # Common setup
  result.variants = variants
  result.help = help
  result.n = n
  result.required = required

  # Check variants are acceptable
  var optional = false
  var positional = false
  if len(variants)==0:
    raise newException(ValueError, "No variants provided")
  for variant in variants:
    if variant.startsWith(parser.longPrefix):
      optional = true
    elif variant.startsWith($parser.shortPrefix):
      optional = true
      if len(variant)!=2:
        raise newException(ValueError, "Short variants must be one letter only (got " & variant & ")")
    elif variant.startsWith($parser.leftBracket) and variant.endsWith($parser.rightBracket):
      positional = true
    else:
      raise newException(ValueError, "Variants must be either of the form " & $parser.shortPrefix & "o or " &
                                      parser.longPrefix & "option if they are options or " & parser.leftBracket & "argument" & parser.rightBracket &
                                      " if they are arguments (got " & variant & ")")
  if positional and optional:
    raise newException(ValueError, "Option cannot be supplied with both positional and optional variants: " & $variants)
  elif positional:
    case result.valueKind:
      of ValueKind.vkCommand:
        result.optionKind = OptionKind.okCommand
      else:
        result.optionKind = OptionKind.okPositional
  elif optional:
    result.optionKind = OptionKind.okOption
  else:
    raise newException(ValueError, "Not sure what kind of an option this is: " & $variants)

  # Assign a name
  if len(name)>0:
    if name.startsWith(parser.longPrefix) or name.startsWith($parser.shortPrefix):
      raise newException(ValueError, "Names cannot start with the long prefix " & parser.longPrefix & " or the short prefix " & $parser.shortPrefix & " got " & name)
    else:
      result.name = name
  elif variants[0].startsWith(parser.longPrefix):
    result.name = variants[0][len(parser.longPrefix)..variants[0].len-1]
  elif variants[0].startsWith($parser.shortPrefix):
    result.name = variants[0][1..variants[0].len-1]
  elif variants[0].startsWith($parser.leftBracket) and variants[0].endsWith($parser.rightBracket):
    result.name = variants[0][1..variants[0].len-2]
  else:
    raise newException(ValueError, "Could not determine name from " & variants[0])

func add*[T: Value](parser: var Parser, variants: seq[string], help: string, default: T,
                                      choices: seq[T] = newSeq[T](), name = "", n = 1, required = RequiredKind.rkDefault) =
  ## Adds an option to the parser
  ## - ``variants`` - sequence of triggers for this option
  ##    - ``-f``or ``--foo`` imples an optional argument
  ##    -  ``<foo>`` implies a positional argument or sub-command
  ## - ``help`` - help text for this option
  ## - ``default`` - default value for the option
  ## - ``choices`` - acceptable values for the option
  ## - ``name`` - the canonical name for this option (used to fetch its value). Defaults to the text part of the first variant
  ## - ``n`` - how many values this argument takes (0 implies none, -1 implies unlimited, n>0 implies finitely many)
  ## - ``required`` - whether an argument is optional or required
  parser.addOption(parser.newOption(variants, help, default, choices, name, n, required))

func add*[T: Value](parser: var Parser, variant: string, help: string, default: T,
                                        choices: seq[T] = newSeq[T](), name = "", n = 1, required = RequiredKind.rkDefault) =
  parser.add(@[variant], help, default, choices, name, n, required)

func addString*(parser: var Parser, variants: seq[string], help: string, default = "", name = "", choices: seq[string] = newSeq[string](), n = 1, required = RequiredKind.rkDefault) =
  parser.addOption(parser.newOption(variants, help, default, choices, name, n, required))

func addString*(parser: var Parser, variant: string, help: string, default = "", name = "", choices: seq[string] =  newSeq[string](), n = 1, required = RequiredKind.rkDefault) =
  parser.addString(@[variant], help, default, name, choices, n, required)

func addInt*(parser: var Parser, variants: seq[string], help: string, default = 0, name = "", choices: seq[int] =  newSeq[int](), n = 1, required = RequiredKind.rkDefault) =
  parser.addOption(parser.newOption(variants, help, default, choices, name, n, required))

func addInt*(parser: var Parser, variant: string, help: string, default: int = 0, name = "", choices: seq[int] =  newSeq[int](), n = 1, required = RequiredKind.rkDefault) =
  parser.addInt(@[variant], help, default, name, choices, n, required)

func addBool*(parser: var Parser, variants: seq[string], help: string, default: bool, name = "", choices: seq[bool] =  newSeq[bool](), n = 1, required = RequiredKind.rkDefault) =
  parser.addOption(parser.newOption(variants, help, default, choices, name, n, required))

func addBool*(parser: var Parser, variant: string, help: string, default: bool, name = "", choices: seq[bool] =  newSeq[bool](), n = 1, required = RequiredKind.rkDefault) =
  parser.addBool(@[variant], help, default, name, choices, n, required)

func addCount*(parser: var Parser, variants: seq[string], help: string, default: int = 0, name = "", required = RequiredKind.rkDefault) =
  parser.addOption(parser.newOption(variants, help, Count(default), newSeq[Count](), name, 0, required))

func addCount*(parser: var Parser, variant: string, help: string, default: int = 0, name = "", required = RequiredKind.rkDefault) =
  parser.addCount(@[variant], help, default, name, required)

func addCommand*(parser: var Parser, variants: seq[string], help: string, parsers: OrderedTable[string, Parser], default = "", name = "", required = RequiredKind.rkDefault) =
  var option = parser.newOption(variants, help, Command(default), newSeq[Command](), name, n=1, required)
  option.parsers = parsers
  parser.addOption(option)

func addCommand*(parser: var Parser, variant: string, help: string, parsers: OrderedTable[string, Parser], default = "", name = "", required = RequiredKind.rkDefault) =
  parser.addCommand(@[variant], help, parsers, default, name, required)

func addHelp*(parser: var Parser, variants: seq[string], help: string) =
  parser.addOption(parser.newOption(variants, help, Help(false), newSeq[Help](), name = "", n=1, RequiredKind.rkOptional))

proc printHelp*(parser: Parser, command: string = extractFilename(getAppFilename())) =
  var usage = "Usage: " & command
  for option in parser.arguments:
    var repeats = newSeq[string]()
    for i in countup(1, abs(option.n)):
      if len(option.variants)>1:
        repeats.add("(" & option.variants.join("|") & ")")
      else:
        repeats.add(option.variants[0])
    if option.n < 0:
        repeats.add("...")
    if option.required == RequiredKind.rkOptional:
      usage &= " [" & repeats.join(" ") & "]"
    else:
      usage &= " " & repeats.join(" ")
  echo(usage)
  if len(parser.prolog)>0:
    echo(wrapWords(unindent(parser.prolog)))
  if len(parser.options)>0:
    var longestoption = 0

    for o in parser.options.values:
      let optionString = parser.optionString(o)
      if len(optionString)>longestoption:
        longestoption = len(optionString)

    var previous: Option
    const indent= "  "
    const indentsize = len(indent)
    var hasArgument = false
    for o in parser.options.values:
      if o!=previous and o.optionKind in {OptionKind.okPositional}:
        if not hasArgument:
          echo("Arguments: ")
          hasArgument = true
        let optionString = parser.optionString(o)
        echo(indent & optionString & spaces(indentsize + longestoption - len(optionString)) & o.optionHelp)
        previous = o
    var hasCommand = false
    for o in parser.options.values:
      if o!=previous and o.optionKind == OptionKind.okCommand:
        if not hasCommand:
          echo("Commands: ")
          hasCommand = true
        let optionString = parser.optionString(o)
        let commandString = optionString & "=" & command
        for command in o.parsers.keys:
          echo(indent & commandString & spaces(indentsize + longestoption - len(commandString)) & o.parsers[command].help)
    var hasOption = false
    for o in parser.options.values:
      if o!=previous and o.optionKind == OptionKind.okOption:
        if not hasOption:
          echo("Options: ")
          hasOption = true
        let optionString = parser.optionString(o)
        echo(indent & optionString & spaces(indentsize + longestoption - len(optionString)) & o.optionHelp)
        previous = o
    if len(parser.epilog)>0:
      echo(wrapWords(unindent(parser.epilog)))

proc parseValues[S: string|TaintedString](option: var Option, variant: S, args: seq[S], pos: var int, shortPrefix: char, longPrefix: string) {. raises: [ParseError, KeyError]}=
  case option.valueKind:
    of vkString, vkInt, vkFloat, vkBool:
      var i = 0
      while i < abs(option.n):
        pos += 1
        if pos>=len(args):
          raise newException(ParseError, "Option " & variant & " expects " & $option.n & " arguments, but " & $i & " provided")
        option.addValue(string(args[pos]))
        i+=1
      if option.n < 0:
        while pos+1 < len(args) and not (args[pos+1].startswith(shortPrefix) or
                                          args[pos+1].startsWith(longPrefix)):
          pos += 1
          option.addValue(string(args[pos]))
    of ValueKind.vkCommand:
      pos += 1
      option.addValue(string(args[pos]))
      pos += 1
      option.parsed[$option.commandVal] = option.parsers[$option.commandVal].parse(args, pos, returnOnComplete = true)
    of vkCount:
      option.countVal += 1
    of vkHelp:
      option.helpVal = true
  when CommandParserDebug:
    debugEcho("Variant " & variant & " => Option " & option.name & " = [" & option.values.join(",") & "] - seen " & $option.count & " times")

proc parse*(
        parser: Parser, 
        args: seq[string]|seq[TaintedString], 
        pos: var int, 
        returnOnComplete = false): ParseResult =
  result.arguments = parser.arguments
  result.options = parser.options 
  var posargs = 0
  var argument: Option = nil
  while pos<len(args):
    if args[pos].startsWith(parser.longPrefix):
      if args[pos] in result.options:
        let variant = args[pos]
        var option = result.options[variant]
        option.count+=1
        case option.valueKind:
          of ValueKind.vkHelp:
            parser.printHelp()
            raise newException(ParseError, "")
          else:
            option.parseValues(variant, args, pos, parser.shortPrefix, parser.longPrefix)
        #echo("Recognised option " & arg & " => " & option.name)
      elif args[pos] == parser.longPrefix & "help":
        try:
          parser.printHelp()
          raise newException(ParseError, "")
        except OSError:
          raise newException(ParseError, "Unable to determine filename: " & getCurrentExceptionMsg())
      else:
        raise newException(ParseError, "Option '" & args[pos] & "' not recognised")
    elif args[pos].startsWith(parser.shortPrefix):
      let arg = args[pos]
      for i in 1..len(arg)-1:
        let variant = parser.shortPrefix & arg[i]
        if variant in result.options:
          var option = result.options[variant]
          option.count+=1
          if option.n>0 and i!=len(arg)-1:
             raise newException(ParseError, "Option " & variant & " takes a value but is not the final option in " & arg)
          case option.valueKind:
            of ValueKind.vkHelp:
              parser.printHelp()
              raise newException(ParseError, "")
            else:
              option.parseValues(variant, args, pos, parser.shortPrefix, parser.longPrefix)
        elif arg[i] == 'h':
          try:
            parser.printHelp()
            raise newException(ParseError, "")
          except OSError:
            raise newException(ParseError, "Unable to determine filename: " & getCurrentExceptionMsg())
        else:
          raise newException(ParseError, "Short option '" & arg[i] & "' not recognised")
    else:
      if posargs<len(result.arguments):
        if isnil(argument) or argument.count>=argument.n:
          argument = result.arguments[posargs]
        argument.count+=1
        pos -= 1 # parse Values will advance by one to read the values
        argument.parseValues(argument.name, args, pos, parser.shortPrefix, parser.longPrefix)
        if len(argument.values) >= argument.n:
          posargs+=1
        if posargs == len(result.arguments) and returnOnComplete:
          return
      else:
        raise newException(ParseError, "Unexpected argument " & args[pos])
    pos += 1
  if posargs<len(result.arguments):
    for arg in parser.arguments[posargs..len(parser.arguments)-1]:
      if arg.required != RequiredKind.rkOptional:
        raise newException(ParseError, "Syntax error: missing argument " & $parser.leftbracket & result.arguments[posargs].name & $parser.rightBracket)

proc parse*(parser: Parser, args: seq[string]|seq[TaintedString]): ParseResult =
  var pos = 0
  parser.parse(args, pos)

proc parse*(parser: Parser, args: string|TaintedString): ParseResult =
  parser.parse(args.split())

proc parse*(parser: Parser): ParseResult =
  try:
    result = parser.parse(commandLineParams())
  except KeyError:
    quit(getCurrentExceptionMsg())
  except ParseError:
    let msg = getCurrentExceptionMsg()
    if len(msg)>0:
      quit(msg)
    else:
      quit()