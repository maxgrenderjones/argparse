import strutils
import tables
import unittest

import ../src/argparse

when isMainModule:
  suite "ArgParse tests":
    test "Short and long prefixes must be different":
      expect(ValueError):
        discard newParser(prolog="intro", epilog="outtro", shortPrefix='-', longPrefix="-")


  suite "Argument setup":
    var parser = newParser(prolog="A demonstration program", epilog="Time to try it yourself?")

    #Check argument pre-checks work
    test "Short options must be one letter":
      expect(ValueError):
        parser.addInt("-bar", help="int value")
    test "Variants must be either optional or positional (not both)":
      expect(ValueError):
        parser.addInt(@["--bar", "<bar>"], help="int value")
    test "Bare arguments are not allowed":
      expect(ValueError):
        parser.addInt(@["bar"], help="int value")

    #Add some normal arguments
    parser.addString(@["--bar"], help="path to the nearest bar (checkd)")
    parser.addInt(@["--foo", "-n"], help="how many foos to use", default=0)
    parser.addBool(@["--quux"], help="Whether to quux", default=false)
    parser.addCount(@["-v", "--verbosity", "--noisiness"], help="increase output")
    parser.addString("--default", help="default value", default="default")

    # Check adding duplicate arguments is not allowed
    test "Duplicate arguments are not allowed":
      expect(ValueError):
       parser.addInt("--bar", help="int value")

    # Check we can parse optional arguments
    let pr = parser.parse("--bar bar --foo 1 --quux on --verbosity".split())
    test "String parsing":
      check(pr.getString("--bar")=="bar")
    test "Int parsing":
      check(pr.getInt("--foo") == 1)
    test "Bool parsing":
      check(pr.getBool("--quux") == true)
    test "Count parsing":
      check(pr.getCount("--verbosity") == 1)
    test "Cam get values using different variant from the one supplied":
      check(pr.getCount("--noisiness") == 1)
    test "Default values are set":
      check(pr.getString("--default") == "default")
    test "Parser knows if option was provided":  
      check(pr.hasOption("--quux") == true)
    test "Parser knows if option was not provided":
      check(pr.hasOption("--default") == false)
    test "Getting non-existent options raise ValueError via get<type>":
      expect(ValueError):
        discard pr.getString("--unicorn")
    test "Getting non-existent options raise KeyError via hasOption":
      expect(KeyError):
        discard pr.hasOption("--unicorn")

    # Check that unparsable arguments fail as expected
    test "Parser complains when given an unparseable int":
      expect(ParseError):
        parser = newParser(prolog="A demonstration program", epilog="Time to try it yourself?")
        parser.addInt(@["--foo"], help="Some int value")
        discard parser.parse("--foo foo --foo foo".split())

    test "Choices are enforced":
      expect(ParseError):
        parser = newParser(prolog="A demonstration program", epilog="Time to try it yourself?")
        parser.addString(@["--colour"], help="Your favourite colour", choices= @["red", "green", "blue"])
        discard parser.parse("--colour fish".split())

  suite "Type parsing":
    var typeParser = newParser(prolog="intro", epilog="outtro")
    typeParser.add("--bool", help="Some boolean value", default=false)
    typeParser.add("--int", help="Some int value", default=0)
    typeParser.add("--float", help="Some float value", default=0.0)
    typeParser.add("--string", help="Some string value", default="")

    let tpr = typeParser.parse("--bool true --int 1 --float 2 --string hello")

    test "Can get and set a bool value":
      check(tpr.getBool("--bool") == true)
    test "Can get and set a int value":
      check(tpr.getInt("--int") == 1)
    test "Can get and set a float value":
      check(tpr.getFloat("--float") == 2.0)
    test "Can get and set a string value":
      check(tpr.getString("--string") == "hello")

  suite "Command Parser":

    var parserWithCommands = newParser(prolog="intro", epilog="outtro")

    var subParser = newParser(help="Copy command", prolog="Help for the copy command")
    subParser.addString(@["<commandArgument>"], help="Command Argument")

    parserWithCommands.addCommand(@["<command>"], help="Function to perform", parsers={"sub": subParser}.toOrderedTable())
    parserWithCommands.addString(@["<another>"], help="post parser command")

    test "Rejects unexpected arguments":
      expect(ParseError):
        discard parserWithCommands.parse("foo".split())

    let pr = parserWithCommands.parse("sub subArgument main".split())

    test "Know when a command has been provided":
      check(pr.hasOption("<command>"))
    test "Can parse commands":
      check(pr.getCommand("<command>") == "sub")
    test "Can parser arguments with subparser":
      check(pr.getParseResult("<command>").getString("<commandArgument>") == "subArgument")
    test "Returns to main parser after subparser":
      check(pr.getString("<another>") == "main")

  suite "Copy command":
    var cpParser = newParser(prolog="Imaginary copy command")
    cpParser.addString("<from>", help="Source file")
    cpParser.addString("<to>", help="Destination")
    cpParser.addString("<suffix>", help="Suffix to add to copied files", default=".bak", required=RequiredKind.rkOptional)
    test "Cannot have required positional arguments after optional one":
      expect(ValueError):
        cpParser.addString("<prefix>", help="Prefix to add to copied files", default="pre")
    test "Can have optional arguments after other optional arguments":
      #  expectNoException("Can have optional positional arguments after optional ones"):
      cpParser.addString("<prefix>", help="Prefix to add to copied files", default="pre", required=RequiredKind.rkOptional)

    test "Check for expected positional arguments":
      expect(ParseError):
        discard cpParser.parse("src")

    let pr = cpParser.parse("src dst")
    test "Positional arguments can be optional":
      check(pr.hasOption("<suffix>") == false)
    test "Optional positional arguments return their default value":
      check(pr.getString("<suffix>") == ".bak")

  # Features to test for
  # - arguments cannot be positional and optional
  # - shortPrefix cannot start with longPrefix
  # - variants cannot be nil or empty
  # - options cannot be used twice

  # Not yet implemented
  # - mutually exclusive options
  # - Line-wrapped help text
  # - Multi-valued positional arguments that aren't last e.g. cp <file1> <file2> <file3> <destination>
  # - Flags (like counts, but either present or not present)
