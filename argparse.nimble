# vim: set tabstop=2 shiftwidth=2 expandtab:

# [Package]
version       = "0.1.0"
author        = "Max Grender-Jones"
description   = "Command line argument parser for nim"
license       = "LGPL"

# [Deps]
requires      "nim >= 0.19.0"

# Config
srcDir = "src"


import strformat, os

task docs, "Builds documentation":
    let builddir = "build" / "docs"
    mkDir builddir
    selfExec fmt"doc --hints:off --outdir:{builddir} src/argparse"